package dao;

import model.Person;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by evelin.jogi on 13.10.2018.
 */
@Repository
public class PersonDao {
    private Long counter = 1L;

    private List<Person> people = new ArrayList<>();

    @PostConstruct
    public void init() {
        people.add(new Person(counter++, "Jack", "Smith", "01.01.1991", "Jack@Jack.com", "Street"));
        people.add(new Person(counter++, "Jill", "Smith", "01.01.1991", "Jill@Jill.com", "Street"));
    }

    public void save(Person person) {
        if (person.getId() != null) {
            deletePersonForId(person.getId());
        } else {
            person.setId(counter++);
        }
        people.add(person);
    }

    public void edit(Long personId, Person person) {
        Person p = getPersonForId(personId);
        person.setId(p.getId());
        deletePersonForId(p.getId());
        people.add(person);
    }

    public void search(String key) {

        people = people.stream()
                .filter(p -> p.getFirstname().contains(key) || p.getLastname().contains(key))
                .collect(Collectors.toList());

    }

    public Person getPersonForId(Long personId) {
        return people.stream()
                .filter(p -> personId.equals(p.getId()))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("can't find: " + personId));
    }

    public List<Person> findAll() {
        return Collections.unmodifiableList(people);
    }

    //Kustutamise jaoks filtreerime listist id järgi välja
    public void deletePersonForId(Long personId) {
        people = people.stream()
                .filter(p -> !personId.equals(p.getId()))
                .collect(Collectors.toList());
    }

}
