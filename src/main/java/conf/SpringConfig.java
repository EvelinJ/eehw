package conf;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created by evelin.jogi on 13.10.2018.
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"controller", "dao"})
public class SpringConfig implements WebMvcConfigurer {
}
