package controller;

import dao.PersonDao;
import model.Person;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by evelin.jogi on 13.10.2018.
 */
@RestController
public class PersonController {

    @Resource
    private PersonDao dao;

    @GetMapping("people/search")
    public void search(@RequestParam(defaultValue = "") String key) {
        dao.search(key);
    }

    @GetMapping("people")
    public List<Person> getPeople() {
        return dao.findAll();
    }

    @GetMapping("people/{id}")
    public Person getPerson(@PathVariable Long id) {
        return dao.getPersonForId(id);
    }

    @PostMapping("people")
    public void savePerson(@RequestBody Person person) {
        dao.save(person);
    }

    @PutMapping("people/{id}")
    public void editPerson(@PathVariable Long id, @RequestBody Person person) {
        dao.edit(id, person);

    }

    @DeleteMapping("people/{id}")
    public void deletePerson(@PathVariable Long id) {
        dao.deletePersonForId(id);
    }
}
