package model;

import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by evelin.jogi on 13.10.2018.
 */
//@NoArgsConstructor
//@AllArgsConstructor
//@Data
public class Person {

    private Long id;
    private String firstname;
    private String lastname;
    private String birthday;
    private String email;
    private String address;

    public Person() {
    }

    public Person(Long id, String firstname, String lastname, String birthday, String email, String address) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.birthday = birthday;
        this.email = email;
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
