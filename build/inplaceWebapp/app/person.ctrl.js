(function () {
    'use strict';

    angular.module('app').controller('PersonCtrl', Ctrl);

    function Ctrl(http, $filter) {

        var vm = this;
        vm.insertNew = insertNew;
        vm.editPerson = editPerson;
        vm.deletePerson = deletePerson;
        vm.sortBy = sortBy;

        vm.newPerson = {};
        vm.people = [];

        vm.propertyName = 'firstname';
        vm.reverse = false;

        init();

        function init() {
            http.get('api/people').then(function (data) {
                vm.people = data;
            });
            vm.people = $filter('orderBy')(vm.people, vm.propertyName, vm.reverse);
        }

        function insertNew(person) {
            var error;
            var today = new Date();
            today.setHours(0,0,0,0);
            var emailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

            if (!vm.newPerson.firstname || !vm.newPerson.lastname || !vm.newPerson.birthday || !vm.newPerson.email || !vm.newPerson.address) {
                error = "Kõik väljad peavad olema täidetud";
                document.getElementById("error").innerHTML = error;
                return false;
            }

            if (vm.people.filter(item => item.firstname.toLowerCase() == vm.newPerson.firstname.toLowerCase() && item.lastname.toLowerCase() == vm.newPerson.lastname.toLowerCase()).length != 0) {
                error = "Selle eesnime ja perenimega isik on juba sisestatud";
                document.getElementById("error").innerHTML = error;
                return false;
            }

            if (vm.newPerson.birthday > today) {
                error = "Sünniaeg ei tohi olla tulevikus";
                document.getElementById("error").innerHTML = error;
                return false;
            }

            if (!vm.newPerson.email.match(emailFormat)) {
                error = "Sisestatud email ei ole korrektne";
                document.getElementById("error").innerHTML = error;
                return false;
            }

            document.getElementById("error").innerHTML = "";

            if (person.id) {
                var method = http.post;
                var url = 'api/people';
            } else {
                var method = http.post;
                var url = 'api/people';
            }

            method(url, vm.newPerson).then(function () {
                vm.newPerson = {};
                init();
            }, errorHandler);
        }

        function editPerson(personId) {
            http.get('api/people/' + personId).then(function (data) {
                data.birthday = new Date(data.birthday);
                vm.newPerson = data;
            });
        }

        function deletePerson(personId) {
            http.delete('api/people/' + personId).then(function () {
                init();
            }, errorHandler);
        }

        function sortBy(propertyName) {
            vm.reverse = (propertyName !== null && vm.propertyName === propertyName)
                ? !vm.reverse : false;
            vm.propertyName = propertyName;
            vm.people = $filter('orderBy')(vm.people, vm.propertyName, vm.reverse);
        }

        function errorHandler(response) {
            console.log('Error: ' + JSON.stringify(response.data));
        }
    }

})();
