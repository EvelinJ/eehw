(function () {
    'use strict';

    var messages = {
        'NotNull.person.firstname' : 'Person firstname is missing',
        'NotNull.person.lastname' : 'Person lastname is missing',
        'Size.person.firstname' : 'Firstname must be between {2} and {1} characters',
        'Size.person.lastname' : 'Lastname must be between {2} and {1} characters'
    };

    angular.module('app').service('messageService', Srv);

    function Srv() {

        this.getMessage = getMessage;

        function getMessage(key, params) {
            var text = messages[key];

            if (text === undefined) {
                return "Unknown message key: " + key;
            }

            if (params === undefined) {
                return text;
            }

            for (var i = 0; i < params.length; i++) {
                text = text.replace(new RegExp('\\{' + (i + 1) + '\\}', 'g'), params[i]);
            }

            return text;
        }

    }

})();
